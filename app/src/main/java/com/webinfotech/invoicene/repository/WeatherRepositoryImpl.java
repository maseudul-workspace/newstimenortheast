package com.webinfotech.invoicene.repository;

import android.util.Log;

import com.google.gson.Gson;
import com.webinfotech.invoicene.domain.model.HomeDataWrapper;
import com.webinfotech.invoicene.domain.model.WeatherWrapper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class WeatherRepositoryImpl {

    WeatherRepository mRepository;

    public WeatherRepositoryImpl() {
        mRepository = WeatherApiClient.createService(WeatherRepository.class);
    }

    public WeatherWrapper fetchWeatherData(double lat,
                                          double lon,
                                          String apiId) {
        WeatherWrapper weatherWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchWeatherData(lat, lon, apiId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    weatherWrapper = null;
                }else{
                    weatherWrapper = gson.fromJson(responseBody, WeatherWrapper.class);
                }
            } else {
                weatherWrapper = null;
            }
        }catch (Exception e){
            weatherWrapper = null;
        }
        return  weatherWrapper;
    }

}
