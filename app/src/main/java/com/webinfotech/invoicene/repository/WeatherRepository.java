package com.webinfotech.invoicene.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WeatherRepository {

    @GET("weather")
    Call<ResponseBody> fetchWeatherData(@Query("lat") double lat,
                                        @Query("lon") double lon,
                                        @Query("appid") String appId);

}
