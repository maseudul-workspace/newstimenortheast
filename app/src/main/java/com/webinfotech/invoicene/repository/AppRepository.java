package com.webinfotech.invoicene.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AppRepository {

    @GET("home/data/{type}")
    Call<ResponseBody> fetchHomeData(@Path("type") int type);

    @GET("post/{type}/{category}/{page}")
    Call<ResponseBody> fetchNewsList(@Path("type") int type,
                                     @Path("category") int category,
                                     @Path("page") int page
                                     );

    @GET("video/list/{type}/{page}")
    Call<ResponseBody> fetchVideoList(  @Path("type") int type,
                                        @Path("page") int page
    );

    @GET("post/details/{id}")
    Call<ResponseBody> fetchNewsDetails(@Path("id") int id);

    @GET("post/search/{searchKey}")
    Call<ResponseBody> fetchSearchNews(@Path("searchKey") String searchKey);

    @POST("device/id")
    @FormUrlEncoded
    Call<ResponseBody> updateFirebaseToken(@Field("device_id") String apiToken);

}
