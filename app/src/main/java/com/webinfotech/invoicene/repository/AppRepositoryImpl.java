package com.webinfotech.invoicene.repository;

import android.util.Log;

import com.google.gson.Gson;
import com.webinfotech.invoicene.domain.model.CommonResponse;
import com.webinfotech.invoicene.domain.model.HomeDataWrapper;
import com.webinfotech.invoicene.domain.model.NewsDetailsWrapper;
import com.webinfotech.invoicene.domain.model.NewsListWrapper;
import com.webinfotech.invoicene.domain.model.SearchWrapper;
import com.webinfotech.invoicene.domain.model.VideoListWrapper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = ApiClient.createService(AppRepository.class);
    }

    public HomeDataWrapper fetchHomeData(int type) {
        HomeDataWrapper homeDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchHomeData(type);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    homeDataWrapper = null;
                }else{
                    homeDataWrapper = gson.fromJson(responseBody, HomeDataWrapper.class);

                }
            } else {
                homeDataWrapper = null;
            }
        }catch (Exception e){
            homeDataWrapper = null;
        }
        return homeDataWrapper;
    }

    public NewsListWrapper fetchNewsList(int type, int category, int page) {
        NewsListWrapper newsListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchNewsList(type, category, page);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    newsListWrapper = null;
                }else{
                    newsListWrapper = gson.fromJson(responseBody, NewsListWrapper.class);

                }
            } else {
                newsListWrapper = null;
            }
        }catch (Exception e){
            newsListWrapper = null;
        }
        return newsListWrapper;
    }

    public NewsDetailsWrapper fetchNewsDetails(int id) {
        NewsDetailsWrapper newsDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchNewsDetails(id);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    newsDetailsWrapper = null;
                }else{
                    newsDetailsWrapper = gson.fromJson(responseBody, NewsDetailsWrapper.class);
                }
            } else {
                newsDetailsWrapper = null;
            }
        }catch (Exception e){
            newsDetailsWrapper = null;
        }
        return newsDetailsWrapper;
    }

    public VideoListWrapper fetchVideoList(int type, int page) {
        VideoListWrapper videoListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchVideoList(type, page);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    videoListWrapper = null;
                }else{
                    videoListWrapper = gson.fromJson(responseBody, VideoListWrapper.class);
                }
            } else {
                videoListWrapper = null;
            }
        }catch (Exception e){
            videoListWrapper = null;
        }
        return videoListWrapper;
    }

    public SearchWrapper fetchSearchNewsList(String searchKey) {
        SearchWrapper searchWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchSearchNews(searchKey);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    searchWrapper = null;
                }else{
                    searchWrapper = gson.fromJson(responseBody, SearchWrapper.class);
                }
            } else {
                searchWrapper = null;
            }
        }catch (Exception e){
            searchWrapper = null;
        }
        return searchWrapper;
    }

    public CommonResponse updateFirebaseToken(String token) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateFirebaseToken(token);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

}
