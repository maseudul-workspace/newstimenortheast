package com.webinfotech.invoicene.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.invoicene.R;
import com.webinfotech.invoicene.domain.model.NewsDetails;
import com.webinfotech.invoicene.util.GlideHelper;
import com.webinfotech.invoicene.util.Helper;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeViewpagerAdapter extends PagerAdapter {

    public interface Callback {
        void newsCallback(int id);
    }

    Context mContext;
    NewsDetails[] newsDetails;
    Callback mCallback;
    @BindView(R.id.img_view_news_poster)
    ImageView imgViewNewsPoster;
    @BindView(R.id.txt_view_news_title)
    TextView txtViewNewsTitle;
    @BindView(R.id.txt_view_date)
    TextView txtViewNewsDate;

    public HomeViewpagerAdapter(Context mContext, NewsDetails[] newsDetails, Callback mCallback) {
        this.mContext = mContext;
        this.newsDetails = newsDetails;
        this.mCallback = mCallback;
    }

    @Override
    public int getCount() {
        return newsDetails.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.home_viewpager_layout, container, false);
        ButterKnife.bind(this, layout);
        View newsLayout = (View) layout.findViewById(R.id.newsLayout);
        newsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.newsCallback(newsDetails[position].id);
            }
        });
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imgViewNewsPoster, mContext.getResources().getString(R.string.APP_URL) + "/post/" + newsDetails[position].image, 20);
        txtViewNewsTitle.setText(newsDetails[position].title);
        txtViewNewsDate.setText(Helper.changeDataFormat(newsDetails[position].createdAt));
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
