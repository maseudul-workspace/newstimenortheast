package com.webinfotech.invoicene.presentation.ui.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.webinfotech.invoicene.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectLanguage {

    public interface Callback
    {
        void languageEnglish();
        void languageAssamese();
    }
    Callback mCallback;
    Context mContext;
    Activity mActivity;
    View selectLanguage;
    AlertDialog.Builder selectLanguageBuilder;
    AlertDialog alertDialog;

    public SelectLanguage(Callback mCallback, Context mContext, Activity mActivity) {
        this.mCallback = mCallback;
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public  void setUpDialog()
    {
        selectLanguage=mActivity.getLayoutInflater().inflate(R.layout.languages_select,null);
        selectLanguageBuilder=new AlertDialog.Builder(mContext,android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        selectLanguageBuilder.setView(selectLanguage);
        alertDialog=selectLanguageBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        ButterKnife.bind(this,selectLanguage);

    }

    @OnClick(R.id.englishTxt) void englishSelected()
    {
        mCallback.languageEnglish();
    }
    @OnClick(R.id.assameseTxt) void assameseSelected()
    {
        mCallback.languageAssamese();
    }

    public  void showDialog()
    {
        alertDialog.show();
    }
    public void dismisDialog()
    {
        alertDialog.dismiss();
        alertDialog.onBackPressed();

    }
}
