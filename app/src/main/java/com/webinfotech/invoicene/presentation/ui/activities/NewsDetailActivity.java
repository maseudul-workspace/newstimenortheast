package com.webinfotech.invoicene.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
;

import com.webinfotech.invoicene.R;
import com.webinfotech.invoicene.domain.executors.impl.ThreadExecutor;
import com.webinfotech.invoicene.domain.model.NewsDetails;
import com.webinfotech.invoicene.presentation.presenters.NewsDetailsPresenter;
import com.webinfotech.invoicene.presentation.presenters.impl.NewsDetailsPresenterImpl;
import com.webinfotech.invoicene.threading.MainThreadImpl;
import com.webinfotech.invoicene.util.GlideHelper;
import com.webinfotech.invoicene.util.Helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewsDetailActivity extends AppCompatActivity implements NewsDetailsPresenter.View {

    @BindView(R.id.img_view_news_poster)
    ImageView imgViewNewsPoster;
    @BindView(R.id.txt_view_news_title)
    TextView txtViewNewsTitle;
    @BindView(R.id.txt_view_date)
    TextView txtViewDate;
    @BindView(R.id.txt_view_news_desc)
    TextView txtViewNewsDesc;
    @BindView(R.id.txt_view_author)
    TextView txtViewAuthor;
    @BindView(R.id.btn_share)
    View btnShare;
    @BindView(R.id.btn_whatsapp_share)
    View btnWhatsappShare;
    @BindView(R.id.btn_fb_share)
    View btnFbShare;
    @BindView(R.id.btn_instagram_share)
    View btnInstagramShare;
    @BindView(R.id.btn_twitter_share)
    View btnTwitterShare;
    String shareUrl = "";
    NewsDetailsPresenterImpl mPresenter;
    int id;
    ProgressDialog progressDialog;
    boolean isRotate = false;
    boolean isDeeplink = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Uri uri = getIntent().getData();
        if (uri != null) {
            List<String> s = uri.getPathSegments();
            id = Integer.parseInt(s.get(6));
            isDeeplink = true;
        } else {
            id = getIntent().getIntExtra("newsId", 0);
            isDeeplink = false;
        }
        id = getIntent().getIntExtra("newsId", 0);
        ButterKnife.bind(this);
        initialisePresenter();
        mPresenter.fetchNewsDetails(id);
        setUpProgressDialog();
        showLoader();
        hideViews();
    }

    private void initialisePresenter() {
        mPresenter = new NewsDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void hideViews() {
        hideView(btnFbShare);
        hideView(btnInstagramShare);
        hideView(btnTwitterShare);
        hideView(btnWhatsappShare);
    }

    @Override
    public void loadNewsDetails(NewsDetails newsDetails, String shareUrl) {
        this.shareUrl = shareUrl;
        txtViewNewsTitle.setText(newsDetails.title);
        txtViewAuthor.setText("By " + newsDetails.author);
        txtViewNewsDesc.setText(Html.fromHtml(newsDetails.body));
        GlideHelper.setImageView(this, imgViewNewsPoster, getResources().getString(R.string.APP_URL) + "/post/" + newsDetails.image);
        txtViewDate.setText(Helper.changeDataFormat(newsDetails.createdAt));
        getSupportActionBar().setTitle(newsDetails.title);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.btn_share) void onShareClicked() {
        isRotate = rotateFab(btnShare, !isRotate);
        if(isRotate){
            showIn(btnWhatsappShare);
            showIn(btnTwitterShare);
            showIn(btnInstagramShare);
            showIn(btnFbShare);
        }else{
            showOut(btnWhatsappShare);
            showOut(btnTwitterShare);
            showOut(btnInstagramShare);
            showOut(btnFbShare);
        }
    }

    @OnClick(R.id.btn_whatsapp_share) void onWhatsAppShareClicked() {
        Intent whatsappIntent=new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.putExtra(Intent.EXTRA_SUBJECT,"Invoice NE");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT,shareUrl);
        startActivity(Intent.createChooser(whatsappIntent," Share in Whatsapp"));
    }

    @OnClick(R.id.btn_fb_share) void onFbShareClicked() {
        Intent facebookIntent=new Intent(Intent.ACTION_SEND);
        facebookIntent.setType("text/plain");
        facebookIntent.putExtra(Intent.EXTRA_SUBJECT,"Invoice NE");
        facebookIntent.setPackage("com.facebook.katana");
        facebookIntent.putExtra(Intent.EXTRA_TEXT,shareUrl);
        startActivity(Intent.createChooser(facebookIntent," Share in Facebook"));
    }

    @OnClick(R.id.btn_twitter_share) void onTwitterShareClicked() {
        Intent twitterIntent=new Intent(Intent.ACTION_SEND);
        twitterIntent.setType("text/plain");
        twitterIntent.putExtra(Intent.EXTRA_SUBJECT,"Invoice NE");
        twitterIntent.setPackage("com.twitter.android");
        twitterIntent.putExtra(Intent.EXTRA_TEXT, shareUrl);
        startActivity(Intent.createChooser(twitterIntent," Share in Twitter"));
    }

    @OnClick(R.id.btn_instagram_share) void instagramShare() {
        Intent instagramIntent=new Intent(Intent.ACTION_SEND);
        instagramIntent.setType("text/plain");
        instagramIntent.putExtra(Intent.EXTRA_SUBJECT,"Invoice NE");
        instagramIntent.setPackage("com.instagram.android");
        instagramIntent.putExtra(Intent.EXTRA_TEXT,shareUrl);
        startActivity(Intent.createChooser(instagramIntent," Share in Instagram"));
    }

    private void hideView(View v) {
        v.setVisibility(View.GONE);
        v.setTranslationY(v.getHeight());
        v.setAlpha(0f);
    }

    public void showIn(final View v) {
        v.setVisibility(View.VISIBLE);
        v.setAlpha(0f);
        v.setTranslationY(v.getHeight());
        v.animate()
                .setDuration(200)
                .translationY(0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                })
                .alpha(1f)
                .start();
    }
    public void showOut(final View v) {
        v.setVisibility(View.VISIBLE);
        v.setAlpha(1f);
        v.setTranslationY(0);
        v.animate()
                .setDuration(200)
                .translationY(v.getHeight())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        v.setVisibility(View.GONE);
                        super.onAnimationEnd(animation);
                    }
                }).alpha(0f)
                .start();
    }

    private boolean rotateFab(final View v, boolean rotate) {
        v.animate().setDuration(200)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                })
                .rotation(rotate ? 135f : 0f);
        return rotate;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
