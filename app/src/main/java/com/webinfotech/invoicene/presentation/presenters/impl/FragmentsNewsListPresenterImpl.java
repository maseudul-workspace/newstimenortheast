package com.webinfotech.invoicene.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.invoicene.AndroidApplication;
import com.webinfotech.invoicene.domain.executors.Executor;
import com.webinfotech.invoicene.domain.executors.MainThread;
import com.webinfotech.invoicene.domain.interactors.FetchNewsListInteractor;
import com.webinfotech.invoicene.domain.interactors.impl.FetchNewsListInteractorImpl;
import com.webinfotech.invoicene.domain.interactors.impl.FetchWeatherInfoInteractorImpl;
import com.webinfotech.invoicene.domain.model.NewsDetails;
import com.webinfotech.invoicene.presentation.presenters.FragmentsNewsListPresenter;
import com.webinfotech.invoicene.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.invoicene.presentation.ui.adapters.NewsAdapter;
import com.webinfotech.invoicene.repository.AppRepositoryImpl;

public class FragmentsNewsListPresenterImpl extends AbstractPresenter implements    FragmentsNewsListPresenter,
                                                                                    FetchNewsListInteractor.Callback,
                                                                                    NewsAdapter.Callback
{

    Context mContext;
    FragmentsNewsListPresenter.View mView;
    NewsDetails[] newNewsDetails;
    NewsAdapter newsAdapter;

    public FragmentsNewsListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchNewsList(int categoryId, int page, String type) {
        if (type.equals("refresh")) {
            newNewsDetails = null;
        }
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        int languageType = androidApplication.getLanguageType(mContext);
        FetchNewsListInteractorImpl fetchNewsListInteractor = new FetchNewsListInteractorImpl(  mExecutor,
                mMainThread,
                this,
                new AppRepositoryImpl(),
                languageType,
                categoryId,
                page
        );
        fetchNewsListInteractor.execute();
    }

    @Override
    public void onGettingNewsListSuccess(NewsDetails[] newsDetails, int totalPage) {
        if (newsDetails.length > 0){
            NewsDetails[] tempNewsDetails;
            tempNewsDetails = newNewsDetails;
            try {
                int len1 = tempNewsDetails.length;
                int len2 = newsDetails.length;
                newNewsDetails = new NewsDetails[len1 + len2];
                System.arraycopy(tempNewsDetails, 0, newNewsDetails, 0, len1);
                System.arraycopy(newsDetails, 0, newNewsDetails, len1, len2);
                newsAdapter.updateDataSet(newNewsDetails);
            }catch (NullPointerException e){
                newNewsDetails = newsDetails;
                newsAdapter = new NewsAdapter(mContext, newsDetails, this);
                mView.loadNewsListAdapter(newsAdapter, totalPage);
            }
        }
    }

    @Override
    public void onGettingNewsListFail(String errorMsg) {

    }

    @Override
    public void newsCallback(int id) {
        mView.goToNewsDetailsActivity(id);
    }
}
