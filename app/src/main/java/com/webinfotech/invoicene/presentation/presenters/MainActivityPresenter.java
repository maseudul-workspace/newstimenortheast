package com.webinfotech.invoicene.presentation.presenters;

import com.webinfotech.invoicene.domain.model.NewsCategory;
import com.webinfotech.invoicene.domain.model.WeatherWrapper;
import com.webinfotech.invoicene.presentation.ui.adapters.BreakingNewsAdapter;
import com.webinfotech.invoicene.presentation.ui.adapters.HomeViewpagerAdapter;
import com.webinfotech.invoicene.presentation.ui.adapters.NewsAdapter;
import com.webinfotech.invoicene.presentation.ui.adapters.VideoListAdapter;

import java.util.ArrayList;

public interface MainActivityPresenter {
    void fetchHomeData();
    void fetchNewsList(int page, String type);
    void fetchVideoList(int page, String type);
    void getWeatherInfo(double lat, double lon, String apiId);
    void updateFirebaseToken(String token);
    interface View {
        void loadData(ArrayList<NewsCategory> newsCategories, BreakingNewsAdapter breakingNewsAdapter, HomeViewpagerAdapter homeViewpagerAdapter);
        void loadNewsListAdapter(NewsAdapter adapter, int totalPage);
        void loadVideoListAdapter(VideoListAdapter videoListAdapter, int videoTotalPage);
        void hideNewsRecyclerView();
        void showLoader();
        void hideLoader();
        void goToNewsDetailsActivity(int newsId);
        void onVideoClicked(String videoId, String videoTitle);
        void loadWeatherDetails(WeatherWrapper weatherWrapper);
    }
}
