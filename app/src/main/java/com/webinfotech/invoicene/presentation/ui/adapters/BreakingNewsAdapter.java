package com.webinfotech.invoicene.presentation.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.invoicene.R;
import com.webinfotech.invoicene.domain.model.NewsDetails;
import com.webinfotech.invoicene.util.GlideHelper;
import com.webinfotech.invoicene.util.Helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BreakingNewsAdapter extends RecyclerView.Adapter<BreakingNewsAdapter.ViewHolder> {

    public interface Callback
    {
        void newsCallback(int id);
    }


    Context mContext;
    NewsDetails[] newsDetails;
    Callback mCallback;

    public BreakingNewsAdapter(Context mContext, NewsDetails[] newsDetails, Callback mCallback) {
        this.mContext = mContext;
        this.newsDetails = newsDetails;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.recycler_view_news_list_horizontal,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageViewCustomRoundedCorners(mContext,holder.newsImage, mContext.getResources().getString(R.string.APP_URL) + "/post/" + newsDetails[position].image,20);
        holder.titleTxtView.setText(newsDetails[position].title);
        holder.newsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.newsCallback(newsDetails[position].id);
            }
        });
        holder.txtViewDate.setText(Helper.changeDataFormat(newsDetails[position].createdAt));
    }

    @Override
    public int getItemCount() {
        return newsDetails.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_news_title)
        TextView titleTxtView;
        @BindView(R.id.img_view_news_poster)
        ImageView newsImage;
        @BindView(R.id.newsLayout)
        View newsLayout;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
