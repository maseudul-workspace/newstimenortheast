package com.webinfotech.invoicene.presentation.presenters;

import com.webinfotech.invoicene.domain.model.NewsDetails;

public interface NewsDetailsPresenter {
    void fetchNewsDetails(int id);
    interface View {
        void loadNewsDetails(NewsDetails newsDetails, String shareUrl);
        void showLoader();
        void hideLoader();
    }
}
