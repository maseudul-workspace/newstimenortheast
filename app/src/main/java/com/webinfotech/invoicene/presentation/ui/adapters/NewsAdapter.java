package com.webinfotech.invoicene.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.webinfotech.invoicene.R;
import com.webinfotech.invoicene.domain.model.NewsDetails;
import com.webinfotech.invoicene.util.GlideHelper;
import com.webinfotech.invoicene.util.Helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    public interface Callback
    {
        void newsCallback(int id);
    }

    Context mContext;
    NewsDetails[] newsDetails;
    Callback callback;

    public NewsAdapter(Context mContext, NewsDetails[] newsDetails, Callback callback) {
        this.mContext = mContext;
        this.newsDetails = newsDetails;
        this.callback = callback;
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater=LayoutInflater.from(mContext);
        view=inflater.inflate(R.layout.recycler_view_news_list,parent,false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        GlideHelper.setImageViewCustomRoundedCorners(mContext,holder.newsImage, mContext.getResources().getString(R.string.APP_URL) + "/post/" + newsDetails[position].image,1);
        holder.titleTxtView.setText(newsDetails[position].title);
        holder.newsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.newsCallback(newsDetails[position].id);
            }
        });
        holder.txtViewDate.setText(Helper.changeDataFormat(newsDetails[position].createdAt));
    }

    @Override
    public int getItemCount() {
        return newsDetails.length;
    }

    public static class NewsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_news_title)
        TextView titleTxtView;
        @BindView(R.id.img_view_news_poster)
        ImageView newsImage;
        @BindView(R.id.newsLayout)
        LinearLayout newsLayout;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;

        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public void updateDataSet(NewsDetails[] newsDetails) {
        this.newsDetails = newsDetails;
    }

}

