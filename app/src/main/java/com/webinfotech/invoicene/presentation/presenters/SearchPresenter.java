package com.webinfotech.invoicene.presentation.presenters;

import com.webinfotech.invoicene.presentation.ui.adapters.NewsAdapter;

public interface SearchPresenter {
    void fetchNewsList(String searchKey);
    interface View {
        void loadData(NewsAdapter newsAdapter);
        void hideViews();
        void onNewsClicked(int newsId);
    }
}
