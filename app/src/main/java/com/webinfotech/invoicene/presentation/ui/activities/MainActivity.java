package com.webinfotech.invoicene.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;
import com.webinfotech.invoicene.AndroidApplication;
import com.webinfotech.invoicene.R;
import com.webinfotech.invoicene.domain.executors.impl.ThreadExecutor;
import com.webinfotech.invoicene.domain.model.News;
import com.webinfotech.invoicene.domain.model.NewsCategory;
import com.webinfotech.invoicene.domain.model.Temperature;
import com.webinfotech.invoicene.domain.model.Weather;
import com.webinfotech.invoicene.domain.model.WeatherWrapper;
import com.webinfotech.invoicene.presentation.presenters.MainActivityPresenter;
import com.webinfotech.invoicene.presentation.presenters.impl.MainActiivityPresenterImpl;
import com.webinfotech.invoicene.presentation.ui.adapters.BreakingNewsAdapter;
import com.webinfotech.invoicene.presentation.ui.adapters.FragmentsAdapter;
import com.webinfotech.invoicene.presentation.ui.adapters.HomeViewpagerAdapter;
import com.webinfotech.invoicene.presentation.ui.adapters.NewsAdapter;
import com.webinfotech.invoicene.presentation.ui.adapters.VideoListAdapter;
import com.webinfotech.invoicene.presentation.ui.fragments.NewsListFragment;
import com.webinfotech.invoicene.threading.MainThreadImpl;
import com.webinfotech.invoicene.util.GlideHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MainActivityPresenter.View, LocationListener, GoogleApiClient.ConnectionCallbacks,
                                                                                            GoogleApiClient.OnConnectionFailedListener
{

    @BindView(R.id.view_pager_home)
    ViewPager viewPagerHome;
    @BindView(R.id.recycler_view_breaking_news)
    RecyclerView recyclerViewBreakingNews;
    @BindView(R.id.dots_indicator)
    DotsIndicator dotsIndicator;
    @BindView(R.id.recycler_view_videos)
    RecyclerView recyclerViewVideos;
    ArrayList<NewsCategory> categoryList;
    ArrayList<News> newsList;
    MainActiivityPresenterImpl mPresenter;
    LinearLayoutManager layoutManager;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    Boolean isVideoScrolling = false;
    Integer viedoCurrentItems;
    Integer videoTotalItems;
    Integer videoScrollOutItems;
    int videoPageNo = 1;
    int videoTotalPage = 1;
    ProgressDialog progressDialog;
    AndroidApplication androidApplication;
    Location mLastLocation;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    String[] appPremisions = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;
    @BindView(R.id.img_view_weather_icon)
    ImageView imgViewWeatherIcon;
    @BindView(R.id.txt_view_current_city)
    TextView txtViewCurrentCity;
    @BindView(R.id.txt_view_current_temp)
    TextView txtViewCurrentTemp;
    @BindView(R.id.txt_view_temp_range)
    TextView txtViewTempRange;
    @BindView(R.id.txt_view_weather_type)
    TextView txtViewWeatherType;
    @BindView(R.id.layout_language)
    View layoutLanguage;
    @BindView(R.id.txt_view_language)
    TextView txtViewLanguage;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.view_pager_news_list)
    ViewPager viewPagerNewsList;
    @BindView(R.id.tab_layout_categories)
    TabLayout tabLayoutCategories;
    @BindView(R.id.main_scroll_view)
    NestedScrollView scrollView;
    @BindView(R.id.support_btn)
    ExtendedFloatingActionButton supportBtn;
    double latitude = 26.1445;
    double longitude = 91.7362;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu_white);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        supportBtn.shrink();
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        int languageType = androidApplication.getLanguageType(this);
        if (languageType == 1) {
            txtViewLanguage.setText("En");
        } else {
            txtViewLanguage.setText("As");
        }
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchHomeData();
        mPresenter.fetchVideoList(videoPageNo, "refresh");
        if (checkAndRequestPermissions()) {
            buildGoogleApiClient();
        } else {
            mPresenter.getWeatherInfo(latitude, longitude, getResources().getString(R.string.weather_api));
        }
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        mPresenter.updateFirebaseToken(refreshedToken);
    }

    public  void initialisePresenter()
    {
        mPresenter = new MainActiivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadData(ArrayList<NewsCategory> newsCategories, BreakingNewsAdapter breakingNewsAdapter, HomeViewpagerAdapter homeViewpagerAdapter) {

        FragmentsAdapter fragmentsAdapter = new FragmentsAdapter(getSupportFragmentManager());

        for (int i = 0; i < newsCategories.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putInt("categoryId", newsCategories.get(i).id);
            NewsListFragment newsListFragment = new NewsListFragment();
            newsListFragment.setArguments(bundle);
            fragmentsAdapter.AddFragment(newsListFragment, newsCategories.get(i).categoryText);
        }

        viewPagerNewsList.setAdapter(fragmentsAdapter);
        tabLayoutCategories.setupWithViewPager(viewPagerNewsList);

        viewPagerHome.setAdapter(homeViewpagerAdapter);
        dotsIndicator.setViewPager(viewPagerHome);

        recyclerViewBreakingNews.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewBreakingNews.setAdapter(breakingNewsAdapter);

    }

    @Override
    public void loadNewsListAdapter(NewsAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new LinearLayoutManager(this);
    }

    @Override
    public void loadVideoListAdapter(VideoListAdapter videoListAdapter, int videoTotalPage) {
        this.videoTotalPage = videoTotalPage;
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        recyclerViewVideos.setVisibility(View.VISIBLE);
        recyclerViewVideos.setAdapter(videoListAdapter);
        recyclerViewVideos.setLayoutManager(layoutManager);
        recyclerViewVideos.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isVideoScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                viedoCurrentItems = layoutManager.getChildCount();
                videoTotalItems  = layoutManager.getItemCount();
                videoScrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollHorizontally(1))
                {
                    if(videoPageNo < videoTotalPage) {
                        isVideoScrolling = false;
                        videoPageNo = videoPageNo + 1;
                        mPresenter.fetchVideoList(videoPageNo, "");
                    }
                }
            }
        });
    }

    @Override
    public void hideNewsRecyclerView() {
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToNewsDetailsActivity(int newsId) {
        Intent intent = new Intent(this, NewsDetailActivity.class);
        intent.putExtra("newsId", newsId);
        startActivity(intent);
    }

    @Override
    public void onVideoClicked(String videoId, String videoTitle) {
        Intent intent = new Intent(this, VideoPreviewActivity.class);
        intent.putExtra("videoId", videoId);
        intent.putExtra("videoTitle", videoTitle);
        startActivity(intent);
    }

    @Override
    public void loadWeatherDetails(WeatherWrapper weatherWrapper) {
        if (weatherWrapper.name != null || weatherWrapper.name.length() == 0) {
            txtViewCurrentCity.setText(weatherWrapper.name);
        }
        Temperature temperature = weatherWrapper.temperature;
        Weather[] weathers = weatherWrapper.weathers;
        if (temperature != null) {
            txtViewCurrentTemp.setText(Long.toString(Math.round(temperature.temp - 275.15)) + " c");
            txtViewTempRange.setText(Long.toString(Math.round(temperature.tempMin - 275.15)) + " c - " + Long.toString(Math.round(temperature.tempMax - 275.15)) + " c");
        }
        if (weathers != null && weathers.length > 0) {
            txtViewWeatherType.setText(weathers[0].description);
            GlideHelper.setImageView(this, imgViewWeatherIcon, "http://openweathermap.org/img/wn/" + weathers[0].icon +  "@4x.png");
        }
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private boolean checkAndRequestPermissions() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String perm : appPremisions) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0) {
                buildGoogleApiClient();
            } else {
                mPresenter.getWeatherInfo(latitude, longitude, getResources().getString(R.string.weather_api));
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onLocationChanged(Location location) {


        mLastLocation = location;

        mPresenter.getWeatherInfo(location.getLatitude(), location.getLongitude(), getResources().getString(R.string.weather_api));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    public AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    @OnClick(R.id.layout_language) void onLanguageLayoutClicked() {
        PopupMenu popup = new PopupMenu(this, layoutLanguage);
        //inflating menu from xml resource
        popup.inflate(R.menu.option);
        //adding click listener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.assamese:
                        androidApplication = (AndroidApplication) getApplicationContext();
                        androidApplication.setLanguageType(getApplicationContext(), 2);
                        Intent intent1 = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.english:
                        androidApplication = (AndroidApplication) getApplicationContext();
                        androidApplication.setLanguageType(getApplicationContext(), 1);
                        Intent intent2 = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent2);
                        break;
                }
                return false;
            }
        });
        //displaying the popup
        popup.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            scrollView.smoothScrollTo(0, viewPagerNewsList.getTop(), 1000);
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.img_view_search) void onSearchClicked() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.support_btn) void onSupportBtnClicked() {
        if (supportBtn.isExtended()) {
            supportBtn.shrink();
            AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
            int languageType = androidApplication.getLanguageType(this);
            if (languageType == 1) {
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, AboutAssameseActivity.class);
                startActivity(intent);
            }

        } else {
            supportBtn.extend();
        }
    }

}
