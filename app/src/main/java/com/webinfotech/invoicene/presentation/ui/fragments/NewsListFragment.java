package com.webinfotech.invoicene.presentation.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;

import com.webinfotech.invoicene.R;
import com.webinfotech.invoicene.domain.executors.impl.ThreadExecutor;
import com.webinfotech.invoicene.presentation.presenters.FragmentsNewsListPresenter;
import com.webinfotech.invoicene.presentation.presenters.impl.FragmentsNewsListPresenterImpl;
import com.webinfotech.invoicene.presentation.ui.activities.NewsDetailActivity;
import com.webinfotech.invoicene.presentation.ui.adapters.NewsAdapter;
import com.webinfotech.invoicene.threading.MainThreadImpl;

public class NewsListFragment extends Fragment implements FragmentsNewsListPresenter.View {

    @BindView(R.id.recycler_view_news_list)
    RecyclerView recyclerViewNewsList;
    FragmentsNewsListPresenterImpl mPresenter;
    int categoryId;
    Context mContext;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;

    public NewsListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categoryId = getArguments().getInt("categoryId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news_list, container, false);
        ButterKnife.bind(this, view);
        mPresenter.fetchNewsList(categoryId, pageNo, "refresh");
        return view;
    }

    private void initialisePresenter() {
        mPresenter = new FragmentsNewsListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), mContext, this);
    }

    @Override
    public void loadNewsListAdapter(NewsAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerViewNewsList.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        recyclerViewNewsList.setAdapter(adapter);
        recyclerViewNewsList.setLayoutManager(layoutManager);
        recyclerViewNewsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchNewsList(categoryId, pageNo, "");
                    }
                }
            }
        });
    }

    @Override
    public void goToNewsDetailsActivity(int newsId) {
        Intent intent = new Intent(mContext, NewsDetailActivity.class);
        intent.putExtra("newsId", newsId);
        startActivity(intent);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        initialisePresenter();
    }

}