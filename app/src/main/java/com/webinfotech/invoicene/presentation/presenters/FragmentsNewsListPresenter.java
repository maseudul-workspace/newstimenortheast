package com.webinfotech.invoicene.presentation.presenters;

import com.webinfotech.invoicene.presentation.ui.adapters.NewsAdapter;

public interface FragmentsNewsListPresenter {
    void fetchNewsList(int categoryId, int page, String type);
    interface View {
        void loadNewsListAdapter(NewsAdapter adapter, int totalPage);
        void goToNewsDetailsActivity(int newsId);
    }
}
