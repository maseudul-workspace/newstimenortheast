package com.webinfotech.invoicene.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.webinfotech.invoicene.R;
import com.webinfotech.invoicene.domain.executors.impl.ThreadExecutor;
import com.webinfotech.invoicene.presentation.presenters.SearchPresenter;
import com.webinfotech.invoicene.presentation.presenters.impl.SearchPresenterImpl;
import com.webinfotech.invoicene.presentation.ui.adapters.NewsAdapter;
import com.webinfotech.invoicene.threading.MainThreadImpl;

public class SearchActivity extends AppCompatActivity implements SearchPresenter.View {

    @BindView(R.id.recycler_view_news_list)
    RecyclerView recyclerViewNewsList;
    String searchKey = "";
    SearchView searchView;
    SearchPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new SearchPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu) {
        getMenuInflater().inflate( R.menu.search_menu, menu);
        MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        myActionMenuItem.expandActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(!query.isEmpty()){
                    searchKey = query;
                    mPresenter.fetchNewsList(searchKey);
                }
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                if(!s.isEmpty()){
                    searchKey = s;
                    mPresenter.fetchNewsList(searchKey);
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public void loadData(NewsAdapter newsAdapter) {
        recyclerViewNewsList.setVisibility(View.VISIBLE);
        recyclerViewNewsList.setAdapter(newsAdapter);
        recyclerViewNewsList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerViewNewsList.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void hideViews() {
        recyclerViewNewsList.setVisibility(View.GONE);
    }

    @Override
    public void onNewsClicked(int newsId) {
        Intent intent = new Intent(this, NewsDetailActivity.class);
        intent.putExtra("newsId", newsId);
        startActivity(intent);
    }
}