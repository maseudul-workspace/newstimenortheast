package com.webinfotech.invoicene.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.webinfotech.invoicene.AndroidApplication;
import com.webinfotech.invoicene.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChooseLanguageActivity extends AppCompatActivity {

    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_language);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_english) void onEnglishClicked() {
        androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setLanguageType(this, 1);
        goToMainActivity();
    }

    @OnClick(R.id.btn_assamese) void onAssameseClicked() {
        androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setLanguageType(this, 2);
        goToMainActivity();
    }

    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
