package com.webinfotech.invoicene.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.webinfotech.invoicene.R;

public class VideoPreviewActivity extends AppCompatActivity {

    @BindView(R.id.view_player)
    YouTubePlayerView playerView;
    String videoId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_preview);
        ButterKnife.bind(this);
        videoId = getIntent().getStringExtra("videoId");
        String videoTitle = getIntent().getStringExtra("videoTitle");
        getSupportActionBar().setTitle(videoTitle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setPlayerView();
    }

    private void setPlayerView() {
        playerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(YouTubePlayer youTubePlayer) {
                super.onReady(youTubePlayer);
                youTubePlayer.loadVideo(videoId, 0);
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        playerView.release();
    }

    @Override
    protected void onPause() {
        super.onPause();
        playerView.release();
    }
}