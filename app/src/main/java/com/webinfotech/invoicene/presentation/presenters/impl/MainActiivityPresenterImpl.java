package com.webinfotech.invoicene.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.invoicene.AndroidApplication;
import com.webinfotech.invoicene.domain.executors.Executor;
import com.webinfotech.invoicene.domain.executors.MainThread;
import com.webinfotech.invoicene.domain.interactors.FetchHomeDataInteractor;
import com.webinfotech.invoicene.domain.interactors.FetchNewsListInteractor;
import com.webinfotech.invoicene.domain.interactors.FetchVideoListInteractor;
import com.webinfotech.invoicene.domain.interactors.FetchWeatherInfoInteractor;
import com.webinfotech.invoicene.domain.interactors.UpdateFirebaseInteractor;
import com.webinfotech.invoicene.domain.interactors.impl.FetchHomeDataInteractorImpl;
import com.webinfotech.invoicene.domain.interactors.impl.FetchNewsListInteractorImpl;
import com.webinfotech.invoicene.domain.interactors.impl.FetchVideoListInteractorImpl;
import com.webinfotech.invoicene.domain.interactors.impl.FetchWeatherInfoInteractorImpl;
import com.webinfotech.invoicene.domain.interactors.impl.UpdateFirebaseInteractorImpl;
import com.webinfotech.invoicene.domain.model.HomeData;
import com.webinfotech.invoicene.domain.model.NewsCategory;
import com.webinfotech.invoicene.domain.model.NewsDetails;
import com.webinfotech.invoicene.domain.model.VideoList;
import com.webinfotech.invoicene.domain.model.WeatherWrapper;
import com.webinfotech.invoicene.presentation.presenters.MainActivityPresenter;
import com.webinfotech.invoicene.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.invoicene.presentation.ui.adapters.BreakingNewsAdapter;
import com.webinfotech.invoicene.presentation.ui.adapters.HomeViewpagerAdapter;
import com.webinfotech.invoicene.presentation.ui.adapters.NewsAdapter;
import com.webinfotech.invoicene.presentation.ui.adapters.VideoListAdapter;
import com.webinfotech.invoicene.repository.AppRepositoryImpl;
import com.webinfotech.invoicene.repository.WeatherRepositoryImpl;

import java.util.ArrayList;

public class MainActiivityPresenterImpl extends AbstractPresenter implements    MainActivityPresenter,
                                                                                FetchHomeDataInteractor.Callback,
                                                                                BreakingNewsAdapter.Callback,
                                                                                HomeViewpagerAdapter.Callback,
                                                                                FetchNewsListInteractor.Callback,
                                                                                FetchVideoListInteractor.Callback,
                                                                                NewsAdapter.Callback,
                                                                                VideoListAdapter.Callback,
                                                                                FetchWeatherInfoInteractor.Callback,
                                                                                UpdateFirebaseInteractor.Callback

{

    Context mContext;
    MainActivityPresenter.View mView;
    FetchNewsListInteractorImpl fetchNewsListInteractor;
    ArrayList<NewsCategory> newsCategories;
    NewsAdapter newsAdapter;
    VideoListAdapter videoListAdapter;
    NewsDetails[] newNewsDetails;
    VideoList[] newVideoList;
    int categoryId;

    public MainActiivityPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchHomeData() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        int languageType = androidApplication.getLanguageType(mContext);
        FetchHomeDataInteractorImpl fetchHomeDataInteractor = new FetchHomeDataInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, languageType);
        fetchHomeDataInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void fetchNewsList(int page, String type) {
        if (type.equals("refresh")) {
            newNewsDetails = null;
        }
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        int languageType = androidApplication.getLanguageType(mContext);
        fetchNewsListInteractor = new FetchNewsListInteractorImpl(  mExecutor,
                                                                    mMainThread,
                                                                    this,
                                                                    new AppRepositoryImpl(),
                                                                    languageType,
                                                                    categoryId,
                                                                    page
                );
        fetchNewsListInteractor.execute();
    }

    @Override
    public void fetchVideoList(int page, String type) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        int languageType = androidApplication.getLanguageType(mContext);
        FetchVideoListInteractorImpl fetchVideoListInteractor = new FetchVideoListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, languageType, page);
        fetchVideoListInteractor.execute();
    }

    @Override
    public void getWeatherInfo(double lat, double lon, String apiId) {
        FetchWeatherInfoInteractorImpl fetchWeatherInfoInteractor = new FetchWeatherInfoInteractorImpl(mExecutor, mMainThread, new WeatherRepositoryImpl(), this, lat, lon, apiId);
        fetchWeatherInfoInteractor.execute();
    }

    @Override
    public void updateFirebaseToken(String token) {
        UpdateFirebaseInteractorImpl updateFirebaseInteractor = new UpdateFirebaseInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), token);
        updateFirebaseInteractor.execute();
    }

    @Override
    public void onGettingHomeDataSuccess(HomeData homeData) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        int languageType = androidApplication.getLanguageType(mContext);
        newsCategories = new ArrayList<>();
        for (int i = 0; i < homeData.mainCategories.length; i++) {
            String category = "";
            if (languageType == 1) {
                category = homeData.mainCategories[i].categoryName;
            } else {
                category = homeData.mainCategories[i].assameseCategoryName;
            }
            newsCategories.add(new NewsCategory(
                    homeData.mainCategories[i].id,
                    category,
                    false,
                    1
            ));
        }
        HomeViewpagerAdapter homeViewpagerAdapter = new HomeViewpagerAdapter(mContext, homeData.sliders, this);
        BreakingNewsAdapter breakingNewsAdapter = new BreakingNewsAdapter(mContext, homeData.breakingNews, this);
        mView.loadData(newsCategories, breakingNewsAdapter, homeViewpagerAdapter);
        categoryId = homeData.mainCategories[0].id;
        fetchNewsList(1, "refresh");
    }

    @Override
    public void onGettingHomeDataFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGettingNewsListSuccess(NewsDetails[] newsDetails, int totalPage) {
        if (newsDetails.length > 0){
            NewsDetails[] tempNewsDetails;
            tempNewsDetails = newNewsDetails;
            try {
                int len1 = tempNewsDetails.length;
                int len2 = newsDetails.length;
                newNewsDetails = new NewsDetails[len1 + len2];
                System.arraycopy(tempNewsDetails, 0, newNewsDetails, 0, len1);
                System.arraycopy(newsDetails, 0, newNewsDetails, len1, len2);
                newsAdapter.updateDataSet(newNewsDetails);
            }catch (NullPointerException e){
                newNewsDetails = newsDetails;
                newsAdapter = new NewsAdapter(mContext, newsDetails, this);
                mView.loadNewsListAdapter(newsAdapter, totalPage);
            }
        } else {
            mView.hideNewsRecyclerView();
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingNewsListFail(String errorMsg) {
        mView.hideNewsRecyclerView();
        mView.hideLoader();
    }

    @Override
    public void newsCallback(int id) {
        mView.goToNewsDetailsActivity(id);
    }

    @Override
    public void onGettingVideoListSuccess(VideoList[] videoLists, int totalPage) {
        if (videoLists.length > 0){
            VideoList[] tempVideoList;
            tempVideoList = newVideoList;
            try {
                int len1 = tempVideoList.length;
                int len2 = videoLists.length;
                newVideoList = new VideoList[len1 + len2];
                System.arraycopy(tempVideoList, 0, newVideoList, 0, len1);
                System.arraycopy(videoLists, 0, newVideoList, len1, len2);
                videoListAdapter.updateDataSet(newVideoList);
            }catch (NullPointerException e){
                newVideoList = videoLists;
                videoListAdapter = new VideoListAdapter(mContext, videoLists, this);
                mView.loadVideoListAdapter(videoListAdapter, totalPage);
            }
        }
    }

    @Override
    public void onGettingVideoListFail(String errorMsg) {

    }

    @Override
    public void onVideoClicked(String videoId, String videoTitle) {
        mView.onVideoClicked(videoId, videoTitle);
    }

    @Override
    public void onGettingWeatherInfoSuccess(WeatherWrapper weatherWrapper) {
        mView.loadWeatherDetails(weatherWrapper);
    }

    @Override
    public void onGettingWeatherInfoFail(String errorMsg) {

    }

    @Override
    public void onFirebaseTokenUpdateSuccess() {

    }

    @Override
    public void onFirebaseTokenUpdateFail() {

    }
}
