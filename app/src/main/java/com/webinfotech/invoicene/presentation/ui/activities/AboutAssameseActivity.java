package com.webinfotech.invoicene.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;

import com.webinfotech.invoicene.R;
import com.webinfotech.invoicene.util.GlideHelper;

public class AboutAssameseActivity extends AppCompatActivity {

    @BindView(R.id.img_view_logo)
    ImageView imgViewLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_assamese);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Support");
    }

    @OnClick(R.id.img_view_youtube) void onYoutubeClicked() {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/channel/UCYpRDBixW0TboCUkx1C6ahQ"));
        startActivity(link);
    }

    @OnClick(R.id.img_view_facebook) void onFacebookClicked() {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/invoicene"));
        startActivity(link);
    }

    @OnClick(R.id.img_view_instagram) void onInstagramClicked() {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/invoice.ne/"));
        startActivity(link);
    }

    @OnClick(R.id.img_view_twitter) void onTwitterClicked() {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/InvoiceNe"));
        startActivity(link);
    }

    @OnClick(R.id.img_view_whatsapp) void onWhatsappClicked() {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?phone=+918099134064"));
        startActivity(link);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}