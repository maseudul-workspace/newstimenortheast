package com.webinfotech.invoicene.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.invoicene.domain.executors.Executor;
import com.webinfotech.invoicene.domain.executors.MainThread;
import com.webinfotech.invoicene.domain.interactors.FetchNewsDetailsInteractor;
import com.webinfotech.invoicene.domain.interactors.impl.FetchNewsDetailsInteractorImpl;
import com.webinfotech.invoicene.domain.model.NewsDetails;
import com.webinfotech.invoicene.presentation.presenters.NewsDetailsPresenter;
import com.webinfotech.invoicene.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.invoicene.repository.AppRepositoryImpl;

public class NewsDetailsPresenterImpl extends AbstractPresenter implements NewsDetailsPresenter, FetchNewsDetailsInteractor.Callback {

    Context mContext;
    NewsDetailsPresenter.View mView;
    FetchNewsDetailsInteractorImpl fetchNewsDetailsInteractor;

    public NewsDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchNewsDetails(int id) {
        fetchNewsDetailsInteractor = new FetchNewsDetailsInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), id);
        fetchNewsDetailsInteractor.execute();
    }

    @Override
    public void onGettingNewsDetailsSuccess(NewsDetails newsDetails, String shareUrl) {
        mView.loadNewsDetails(newsDetails, shareUrl);
        mView.hideLoader();
    }

    @Override
    public void onGettingNewsDetailsFail(String errorMsg) {
        mView.hideLoader();
    }
}
