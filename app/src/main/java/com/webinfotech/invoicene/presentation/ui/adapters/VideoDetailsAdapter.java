package com.webinfotech.invoicene.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.webinfotech.invoicene.R;
import com.webinfotech.invoicene.domain.model.Item;
import com.webinfotech.invoicene.util.GlideHelper;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoDetailsAdapter extends RecyclerView.Adapter<VideoDetailsAdapter.myVideoViewHolder> {

    public interface Callback{
        void videoPlayCallback(String videoId);
    }

    Callback callback;
    Context context;
    Item[]  videoDetailsList;


    public VideoDetailsAdapter( Callback callback,Context context, Item[] videoDetailsList) {
        this.callback = callback;
        this.context = context;
        this.videoDetailsList = videoDetailsList;
    }

    @NonNull
    @Override
    public myVideoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view= LayoutInflater.from(context).inflate(R.layout.item_layout,parent,false);
        return new myVideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull myVideoViewHolder holder, int position) {
        holder.itemDate.setText(videoDetailsList[position].snippet.publishedAt);
        holder.itemDescription.setText(videoDetailsList[position].snippet.description);
        holder.itemTitle.setText(videoDetailsList[position].snippet.title);

        holder.itemImgLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.videoPlayCallback(videoDetailsList[position].videoId.videoId);
            }
        });
        // using glide for setting thumbnails
        GlideHelper.setImageView(context,holder.itemImg,videoDetailsList[position].snippet.thumbnails.high.url);
    }

    @Override
    public int getItemCount()
    {
       return videoDetailsList.length;
    }

    public  class  myVideoViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_date)
        TextView itemDate;
        @BindView(R.id.item_title)
        TextView itemTitle;
        @BindView(R.id.item_description)
        TextView itemDescription;
        @BindView(R.id.item_imgLayout)
        LinearLayout itemImgLayout;
        @BindView(R.id.itemImg)
        ImageView itemImg;
        public myVideoViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
