package com.webinfotech.invoicene.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.invoicene.R;
import com.webinfotech.invoicene.domain.model.VideoList;
import com.webinfotech.invoicene.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.ViewHolder> {

    public interface Callback {
        void onVideoClicked(String videoId, String title);
    }

    Context mContext;
    VideoList[] videoLists;
    Callback mCallback;

    public VideoListAdapter(Context mContext, VideoList[] videoLists, Callback mCallback) {
        this.mContext = mContext;
        this.videoLists = videoLists;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.recycler_view_video_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewVideoTitle.setText(videoLists[position].title);
        GlideHelper.setImageViewCustomRoundedCorners(mContext,holder.imgViewThumbnail, mContext.getResources().getString(R.string.APP_URL) + "/youtube/" + videoLists[position].thumbnail,20);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onVideoClicked(videoLists[position].videoId, videoLists[position].title);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videoLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.txt_view_video_title)
        TextView txtViewVideoTitle;
        @BindView(R.id.img_view_video_thumbnail)
        ImageView imgViewThumbnail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public void updateDataSet(VideoList[] videoLists) {
        this.videoLists = videoLists;
    }

}
