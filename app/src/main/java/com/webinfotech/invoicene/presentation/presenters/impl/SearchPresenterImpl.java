package com.webinfotech.invoicene.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.invoicene.domain.executors.Executor;
import com.webinfotech.invoicene.domain.executors.MainThread;
import com.webinfotech.invoicene.domain.interactors.FetchSearchNewsInteractor;
import com.webinfotech.invoicene.domain.interactors.impl.FetchSearchNewsInteractorImpl;
import com.webinfotech.invoicene.domain.model.NewsDetails;
import com.webinfotech.invoicene.presentation.presenters.SearchPresenter;
import com.webinfotech.invoicene.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.invoicene.presentation.ui.adapters.NewsAdapter;
import com.webinfotech.invoicene.repository.AppRepositoryImpl;

public class SearchPresenterImpl extends AbstractPresenter implements   SearchPresenter,
                                                                        FetchSearchNewsInteractor.Callback,
                                                                        NewsAdapter.Callback
{

    Context mContext;
    SearchPresenter.View mView;
    String searchKey;

    public SearchPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchNewsList(String searchKey) {
        this.searchKey = searchKey;
        FetchSearchNewsInteractorImpl fetchSearchNewsInteractor = new FetchSearchNewsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, searchKey);
        fetchSearchNewsInteractor.execute();
    }

    @Override
    public void onGettingNewsListSuccess(NewsDetails[] newsDetails, String searchKey) {
        if (newsDetails.length > 0) {
            if (this.searchKey.equals(searchKey)) {
                NewsAdapter newsAdapter = new NewsAdapter(mContext, newsDetails, this);
                mView.loadData(newsAdapter);
            }
        } else {
            mView.hideViews();
        }
    }

    @Override
    public void onGettingNewsListFail(String errorMsg) {
        mView.hideViews();
    }

    @Override
    public void newsCallback(int id) {
        mView.onNewsClicked(id);
    }
}
