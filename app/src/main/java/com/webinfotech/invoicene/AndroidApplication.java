package com.webinfotech.invoicene;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class AndroidApplication extends Application {

    int languageType = 0;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setLanguageType(Context mContext, int languageType) {
        this.languageType = languageType;
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                getString(R.string.KEY_NEWS_TIME_NORTHEAST_APP_PREFERENCES), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        if(languageType != 0) {
            editor.putString(getString(R.string.KEY_LANGUAGE_TYPE), Integer.toString(languageType));
        } else {
            editor.putString(getString(R.string.KEY_LANGUAGE_TYPE), "");
        }
        editor.commit();
    }

    public int getLanguageType(Context mContext) {
        int languageType;
        if(this.languageType != 0){
            languageType = this.languageType;
        }else{
            SharedPreferences sharedPref = mContext.getSharedPreferences(
                    getString(R.string.KEY_NEWS_TIME_NORTHEAST_APP_PREFERENCES), Context.MODE_PRIVATE);
            String languageString = sharedPref.getString(getString(R.string.KEY_LANGUAGE_TYPE),"");
            if(languageString.isEmpty()){
                languageType = 0;
            }else{
                try {
                    languageType = Integer.parseInt(languageString);
                    this.languageType = languageType;
                }catch (Exception e){
                    languageType = 0;
                }
            }
        }
        return languageType;
    }

}
