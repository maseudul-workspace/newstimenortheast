package com.webinfotech.invoicene.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsDetails {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("body")
    @Expose
    public String body;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("author")
    @Expose
    public String author;

    @SerializedName("created_at")
    @Expose
    public String createdAt;

}
