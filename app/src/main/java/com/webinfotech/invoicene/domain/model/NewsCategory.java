package com.webinfotech.invoicene.domain.model;

public class NewsCategory {

    public int id;
    public String categoryText;
    public boolean isSelected = false;
    public int type;

    public NewsCategory(int id, String categoryText, boolean isSelected, int type) {
        this.id = id;
        this.categoryText = categoryText;
        this.isSelected = isSelected;
        this.type = type;
    }
}
