package com.webinfotech.invoicene.domain.interactors;

import com.webinfotech.invoicene.domain.model.VideoList;

public interface FetchVideoListInteractor {
    interface Callback {
        void onGettingVideoListSuccess(VideoList[] videoLists, int page);
        void onGettingVideoListFail(String errorMsg);
    }
}
