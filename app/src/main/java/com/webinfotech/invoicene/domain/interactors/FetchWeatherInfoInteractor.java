package com.webinfotech.invoicene.domain.interactors;

import com.webinfotech.invoicene.domain.model.WeatherWrapper;

public interface FetchWeatherInfoInteractor {
    interface Callback {
        void onGettingWeatherInfoSuccess(WeatherWrapper weatherWrapper);
        void onGettingWeatherInfoFail(String errorMsg);
    }
}
