package com.webinfotech.invoicene.domain.interactors;

import com.webinfotech.invoicene.domain.model.NewsDetails;

public interface FetchSearchNewsInteractor {
    interface Callback {
        void onGettingNewsListSuccess(NewsDetails[] newsDetails, String searchKey);
        void onGettingNewsListFail(String errorMsg);
    }
}
