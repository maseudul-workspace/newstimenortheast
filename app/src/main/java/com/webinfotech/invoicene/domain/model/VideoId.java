package com.webinfotech.invoicene.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoId {

    @SerializedName("kind")
    @Expose
    public String kind;

    @SerializedName("videoId")
    @Expose
    public String videoId;

}
