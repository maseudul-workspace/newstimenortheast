package com.webinfotech.invoicene.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoList {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("author")
    @Expose
    public String author;

    @SerializedName("thumbnail")
    @Expose
    public String thumbnail;

    @SerializedName("v_id")
    @Expose
    public String videoId;

}
