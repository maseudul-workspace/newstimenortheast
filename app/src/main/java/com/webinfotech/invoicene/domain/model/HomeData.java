package com.webinfotech.invoicene.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeData {

    @SerializedName("slider")
    @Expose
    public NewsDetails[] sliders;

    @SerializedName("breaking")
    @Expose
    public NewsDetails[] breakingNews;

    @SerializedName("category")
    @Expose
    public MainCategory[] mainCategories;

}
