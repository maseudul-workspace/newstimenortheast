package com.webinfotech.invoicene.domain.model;

/**
 * Created by Raj on 29-06-2019.
 */

public class NotificationModel {

    public int newsId;
    public String title;
    public String desc;

    public NotificationModel(int newsId, String title, String desc) {
        this.newsId = newsId;
        this.title = title;
        this.desc = desc;
    }
}
