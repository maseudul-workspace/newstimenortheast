package com.webinfotech.invoicene.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeatherWrapper {

    @SerializedName("cod")
    @Expose
    public int cod;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("weather")
    @Expose
    public Weather[] weathers;

    @SerializedName("main")
    @Expose
    public Temperature temperature;

}
