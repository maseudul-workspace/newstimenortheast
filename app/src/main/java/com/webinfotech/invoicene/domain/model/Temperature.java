package com.webinfotech.invoicene.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Temperature {

    @SerializedName("temp")
    @Expose
    public double temp;

    @SerializedName("temp_min")
    @Expose
    public double tempMin;

    @SerializedName("temp_max")
    @Expose
    public double tempMax;

}
