package com.webinfotech.invoicene.domain.interactors.impl;

import com.webinfotech.invoicene.domain.executors.Executor;
import com.webinfotech.invoicene.domain.executors.MainThread;
import com.webinfotech.invoicene.domain.interactors.FetchHomeDataInteractor;
import com.webinfotech.invoicene.domain.interactors.base.AbstractInteractor;
import com.webinfotech.invoicene.domain.model.HomeData;
import com.webinfotech.invoicene.domain.model.HomeDataWrapper;
import com.webinfotech.invoicene.repository.AppRepositoryImpl;

public class FetchHomeDataInteractorImpl extends AbstractInteractor implements FetchHomeDataInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int type;

    public FetchHomeDataInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int type) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.type = type;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingHomeDataFail(errorMsg);
            }
        });
    }

    private void postMessage(HomeData homeData) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingHomeDataSuccess(homeData);
            }
        });
    }

    @Override
    public void run() {
        final HomeDataWrapper homeDataWrapper = mRepository.fetchHomeData(type);
        if (homeDataWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!homeDataWrapper.status) {
            notifyError(homeDataWrapper.message);
        } else {
            postMessage(homeDataWrapper.homeData);
        }
    }
}
