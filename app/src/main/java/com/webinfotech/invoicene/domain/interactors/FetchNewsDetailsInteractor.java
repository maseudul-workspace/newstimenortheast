package com.webinfotech.invoicene.domain.interactors;

import com.webinfotech.invoicene.domain.model.NewsDetails;

public interface FetchNewsDetailsInteractor {
    interface Callback {
        void onGettingNewsDetailsSuccess(NewsDetails newsDetails, String shareUrl);
        void onGettingNewsDetailsFail(String errorMsg);
    }
}
