package com.webinfotech.invoicene.domain.model;

public class News {

    public int id;
    public String newsImgUrl;
    public String title;
    public String shortDescription;


    public News(int id, String newsImgUrl, String title, String shortDescription) {
        this.id = id;
        this.newsImgUrl = newsImgUrl;
        this.title = title;
        this.shortDescription = shortDescription;
    }
}
