package com.webinfotech.invoicene.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainCategory {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("category_name")
    @Expose
    public String categoryName;

    @SerializedName("as_category_name")
    @Expose
    public String assameseCategoryName;

    @SerializedName("status")
    @Expose
    public int status;

}
