package com.webinfotech.invoicene.domain.interactors.impl;

import com.webinfotech.invoicene.domain.executors.Executor;
import com.webinfotech.invoicene.domain.executors.MainThread;
import com.webinfotech.invoicene.domain.interactors.FetchNewsListInteractor;
import com.webinfotech.invoicene.domain.interactors.base.AbstractInteractor;
import com.webinfotech.invoicene.domain.model.NewsDetails;
import com.webinfotech.invoicene.domain.model.NewsListWrapper;
import com.webinfotech.invoicene.repository.AppRepositoryImpl;

public class FetchNewsListInteractorImpl extends AbstractInteractor implements FetchNewsListInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    int type;
    int category;
    int page;

    public FetchNewsListInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, int type, int category, int page) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.type = type;
        this.category = category;
        this.page = page;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingNewsListFail(errorMsg);
            }
        });
    }

    private void postMessage(final NewsDetails[] newsDetails, int totalPage) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingNewsListSuccess(newsDetails, totalPage);
            }
        });
    }


    @Override
    public void run() {
        final NewsListWrapper newsListWrapper = mRepository.fetchNewsList(type, category, page);
        if (newsListWrapper == null) {
            notifyError("Something Went Wrong");
        } else if (!newsListWrapper.status) {
            notifyError(newsListWrapper.message);
        } else {
            postMessage(newsListWrapper.newsDetails, newsListWrapper.totalPage);
        }
    }
}
