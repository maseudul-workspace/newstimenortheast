package com.webinfotech.invoicene.domain.interactors;

import com.webinfotech.invoicene.domain.model.NewsDetails;

public interface FetchNewsListInteractor {
    interface Callback {
        void onGettingNewsListSuccess(NewsDetails[] newsDetails, int totalPage);
        void onGettingNewsListFail(String errorMsg);
    }
}
