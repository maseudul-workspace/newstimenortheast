package com.webinfotech.invoicene.domain.interactors.impl;

import com.webinfotech.invoicene.domain.executors.Executor;
import com.webinfotech.invoicene.domain.executors.MainThread;
import com.webinfotech.invoicene.domain.interactors.FetchVideoListInteractor;
import com.webinfotech.invoicene.domain.interactors.FetchWeatherInfoInteractor;
import com.webinfotech.invoicene.domain.interactors.base.AbstractInteractor;
import com.webinfotech.invoicene.domain.model.NewsDetails;
import com.webinfotech.invoicene.domain.model.WeatherWrapper;
import com.webinfotech.invoicene.repository.AppRepositoryImpl;
import com.webinfotech.invoicene.repository.WeatherRepositoryImpl;

public class FetchWeatherInfoInteractorImpl extends AbstractInteractor implements FetchWeatherInfoInteractor {

    WeatherRepositoryImpl mRepository;
    Callback mCallback;
    double lat;
    double lon;
    String apiId;

    public FetchWeatherInfoInteractorImpl(Executor threadExecutor, MainThread mainThread, WeatherRepositoryImpl mRepository, Callback mCallback, double lat, double lon, String apiId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.lat = lat;
        this.lon = lon;
        this.apiId = apiId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWeatherInfoFail(errorMsg);
            }
        });
    }

    private void postMessage(final WeatherWrapper weatherWrapper) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWeatherInfoSuccess(weatherWrapper);
            }
        });
    }

    @Override
    public void run() {
        final WeatherWrapper weatherWrapper = mRepository.fetchWeatherData(lat, lon, apiId);
        if (weatherWrapper == null) {
            notifyError("");
        } else if (weatherWrapper.cod == 400) {
            notifyError("");
        } else {
            postMessage(weatherWrapper);
        }
    }

}
