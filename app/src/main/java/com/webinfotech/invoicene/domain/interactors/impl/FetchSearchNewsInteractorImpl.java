package com.webinfotech.invoicene.domain.interactors.impl;

import com.webinfotech.invoicene.domain.executors.Executor;
import com.webinfotech.invoicene.domain.executors.MainThread;
import com.webinfotech.invoicene.domain.interactors.FetchSearchNewsInteractor;
import com.webinfotech.invoicene.domain.interactors.base.AbstractInteractor;
import com.webinfotech.invoicene.domain.model.NewsDetails;
import com.webinfotech.invoicene.domain.model.SearchWrapper;
import com.webinfotech.invoicene.repository.AppRepositoryImpl;

public class FetchSearchNewsInteractorImpl extends AbstractInteractor implements FetchSearchNewsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String searchKey;

    public FetchSearchNewsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String searchKey) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.searchKey = searchKey;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingNewsListFail(errorMsg);
            }
        });
    }

    private void postMessage(final NewsDetails[] newsDetails, String searchKey) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingNewsListSuccess(newsDetails, searchKey);
            }
        });
    }

    @Override
    public void run() {
        final SearchWrapper searchWrapper = mRepository.fetchSearchNewsList(searchKey);
        if (searchWrapper == null) {
            notifyError("");
        } else if (!searchWrapper.status) {
            notifyError("");
        } else {
            postMessage(searchWrapper.newsDetails, searchWrapper.searchKey);
        }
    }
}
