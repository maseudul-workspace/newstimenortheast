package com.webinfotech.invoicene.domain.interactors.impl;


import com.webinfotech.invoicene.domain.executors.Executor;
import com.webinfotech.invoicene.domain.executors.MainThread;
import com.webinfotech.invoicene.domain.interactors.UpdateFirebaseInteractor;
import com.webinfotech.invoicene.domain.interactors.base.AbstractInteractor;
import com.webinfotech.invoicene.domain.model.CommonResponse;
import com.webinfotech.invoicene.repository.AppRepositoryImpl;

public class UpdateFirebaseInteractorImpl extends AbstractInteractor implements UpdateFirebaseInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String token;

    public UpdateFirebaseInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCalback, AppRepositoryImpl mRepository, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCalback;
        this.mRepository = mRepository;
        this.token = token;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFirebaseTokenUpdateFail();
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFirebaseTokenUpdateSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.updateFirebaseToken(token);
        if (commonResponse == null) {
            notifyError();
        } else if (!commonResponse.status) {
            notifyError();
        } else {
            postMessage();
        }
    }
}
