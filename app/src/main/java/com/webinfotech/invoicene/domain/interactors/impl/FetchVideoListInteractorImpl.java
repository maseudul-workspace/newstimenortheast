package com.webinfotech.invoicene.domain.interactors.impl;

import com.webinfotech.invoicene.domain.executors.Executor;
import com.webinfotech.invoicene.domain.executors.MainThread;
import com.webinfotech.invoicene.domain.interactors.FetchVideoListInteractor;
import com.webinfotech.invoicene.domain.interactors.base.AbstractInteractor;
import com.webinfotech.invoicene.domain.model.NewsDetails;
import com.webinfotech.invoicene.domain.model.VideoList;
import com.webinfotech.invoicene.domain.model.VideoListWrapper;
import com.webinfotech.invoicene.repository.AppRepositoryImpl;

public class FetchVideoListInteractorImpl extends AbstractInteractor implements FetchVideoListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int type;
    int pageNo;

    public FetchVideoListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int type, int pageNo) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.type = type;
        this.pageNo = pageNo;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingVideoListFail(errorMsg);
            }
        });
    }

    private void postMessage(VideoList[] videoLists, int totalPage) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingVideoListSuccess(videoLists, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final VideoListWrapper videoListWrapper = mRepository.fetchVideoList(type, pageNo);
        if (videoListWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!videoListWrapper.status) {
            notifyError(videoListWrapper.message);
        } else {
            postMessage(videoListWrapper.videoLists, videoListWrapper.totalPage);
        }
    }
}
