package com.webinfotech.invoicene.domain.interactors;

import com.webinfotech.invoicene.domain.model.HomeData;

public interface FetchHomeDataInteractor {
    interface Callback {
        void onGettingHomeDataSuccess(HomeData homeData);
        void onGettingHomeDataFail(String errorMsg);
    }
}
