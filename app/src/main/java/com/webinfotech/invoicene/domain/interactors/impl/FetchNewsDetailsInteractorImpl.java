package com.webinfotech.invoicene.domain.interactors.impl;

import com.webinfotech.invoicene.domain.executors.Executor;
import com.webinfotech.invoicene.domain.executors.MainThread;
import com.webinfotech.invoicene.domain.interactors.FetchNewsDetailsInteractor;
import com.webinfotech.invoicene.domain.interactors.base.AbstractInteractor;
import com.webinfotech.invoicene.domain.model.NewsDetails;
import com.webinfotech.invoicene.domain.model.NewsDetailsWrapper;
import com.webinfotech.invoicene.repository.AppRepositoryImpl;

public class FetchNewsDetailsInteractorImpl extends AbstractInteractor implements FetchNewsDetailsInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    int id;

    public FetchNewsDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, int id) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.id = id;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingNewsDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(final NewsDetails newsDetails, String shareUrl) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingNewsDetailsSuccess(newsDetails, shareUrl);
            }
        });
    }

    @Override
    public void run() {
        final NewsDetailsWrapper newsDetailsWrapper = mRepository.fetchNewsDetails(id);
        if (newsDetailsWrapper == null) {
            notifyError("Something Went Wrong");
        } else if (!newsDetailsWrapper.status) {
            notifyError(newsDetailsWrapper.message);
        } else {
            postMessage(newsDetailsWrapper.newsDetails, newsDetailsWrapper.shareURL);
        }
    }
}
