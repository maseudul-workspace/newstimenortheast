package com.webinfotech.invoicene.domain.interactors;

public interface UpdateFirebaseInteractor {
    interface Callback {
        void onFirebaseTokenUpdateSuccess();
        void onFirebaseTokenUpdateFail();
    }
}
