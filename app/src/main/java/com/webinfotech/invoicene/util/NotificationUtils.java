package com.webinfotech.invoicene.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RemoteViews;

import com.webinfotech.invoicene.R;
import com.webinfotech.invoicene.domain.model.NotificationModel;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;


/**
 * Created by Raj on 29-06-2019.
 */

public class NotificationUtils {
    private static final int NOTIFICATION_ID = 0;
    private static final String PUSH_NOTIFICATION = "pushNotification";
    private static final String CHANNEL_ID = "myChannel";
    private static final String CHANNEL_NAME = "myChannelName";
    private Context mContext;

    public NotificationUtils(Context mContext) {
        this.mContext = mContext;
    }

    public void displayNotification(NotificationModel model, Intent resultIntent){
        String desc = model.desc;
        String newsTitle = model.title;
        int newsId = model.newsId;
        PendingIntent resultPendingIntent;

        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        resultPendingIntent = PendingIntent.getActivity(
                                mContext,
                                newsId,
                                resultIntent,
                                PendingIntent.FLAG_ONE_SHOT
                        );

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                mContext, CHANNEL_ID);

        Notification notification;


        notification = mBuilder
                .setAutoCancel(true)
                .setContentTitle(newsTitle)
                .setContentText(desc)
                .setSubText("From Invoice NE")
                .setContentIntent(resultPendingIntent)
                .setSmallIcon(R.drawable.transparent_logo)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        //All notifications should go through NotificationChannel on Android 26 & above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);

        }
        notificationManager.notify(newsId, notification);

    }




}
