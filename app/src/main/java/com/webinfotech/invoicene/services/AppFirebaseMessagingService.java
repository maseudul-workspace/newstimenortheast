package com.webinfotech.invoicene.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.text.Html;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.webinfotech.invoicene.AndroidApplication;
import com.webinfotech.invoicene.R;
import com.webinfotech.invoicene.domain.model.NotificationModel;
import com.webinfotech.invoicene.presentation.ui.activities.MainActivity;
import com.webinfotech.invoicene.presentation.ui.activities.NewsDetailActivity;
import com.webinfotech.invoicene.util.NotificationUtils;

public class AppFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        String newsTitle = "";
        String newsDesc = "";
        int newsId = 0;
        try {
            newsTitle = remoteMessage.getData().get("title");
        } catch (NullPointerException e) {
            newsTitle = "News Title";
        }

        try {
            newsDesc = remoteMessage.getData().get("body");
        } catch (NullPointerException e) {
            newsDesc = "News Description";
        }

        try {
            newsId = Integer.parseInt(remoteMessage.getData().get("news_id"));
        } catch (NullPointerException e) {
            newsId = 0;
        }

        Intent resultIntent = new Intent(this, NewsDetailActivity.class);
        resultIntent.putExtra("newsId", newsId);
        NotificationModel notificationModel = new NotificationModel(newsId, newsTitle, Html.fromHtml(newsDesc).toString());
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.displayNotification(notificationModel, resultIntent);
    }

    @Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent){
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());

        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartServicePendingIntent);

        super.onTaskRemoved(rootIntent);
    }

}
